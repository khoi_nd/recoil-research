// craco.config.js
require('dotenv').config();

module.exports = function () {
  return {
    style: {
      postcss: {
        plugins: [require('tailwindcss'), require('autoprefixer')],
      },
    },
  };
};
