export enum RequestStatus {
  SUCCESS = 'hasValue',
  LOADING = 'loading',
  ERROR = 'hasError',
}
