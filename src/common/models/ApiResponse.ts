export type ApiError = {
  code: number;
  errors: ErrorData[];
  message?: string;
  error?: string;
};

export type ErrorData = {
  code: string;
  message: string;
  parameter?: string;
};

export type ListApiResponse<T> = {
  docs: T[];
  limit: number;
  page: number;
  pages: number;
  total: number;
};
