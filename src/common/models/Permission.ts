export interface PermissionList {
  [key: string]: boolean;
}

export enum EPermissionPage {
  Campaigns = 'Campaigns',
  Collections = 'Collections',
  Dashboard = 'Dashboard',
  DynamicConfig = 'DynamicConfig',
  ManageHome = 'ManageHome',
  Orders = 'Orders',
  Riders = 'Riders',
  Stores = 'Stores',
  Users = 'Users',
  Cities = 'Cities',
  OrderInquiries = 'ReportedOrders',
  Payments = 'Payments',
  Frequency = 'Frequency',
  Coupons = 'Coupons',
  SurchargeFee = 'SurchargeFee',
  Settlement = 'Settlement',
  ReviewManagement = 'ReviewManagement',
}

export type PermissionPage =
  | EPermissionPage.Campaigns
  | EPermissionPage.Collections
  | EPermissionPage.Dashboard
  | EPermissionPage.DynamicConfig
  | EPermissionPage.ManageHome
  | EPermissionPage.Orders
  | EPermissionPage.Riders
  | EPermissionPage.Stores
  | EPermissionPage.Users
  | EPermissionPage.Cities
  | EPermissionPage.OrderInquiries
  | EPermissionPage.Payments
  | EPermissionPage.Frequency
  | EPermissionPage.Coupons
  | EPermissionPage.SurchargeFee
  | EPermissionPage.Settlement
  | EPermissionPage.ReviewManagement;
