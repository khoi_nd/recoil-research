import { PermissionList, PermissionPage } from "./Permission";

export type UserInfo = {
  avatar: string;
  email: string;
  name: string;
  permission?: string[];
  permissionsMap?: Record<PermissionPage, PermissionList>
  tokenId?: string;
};

export type UserInfoBasic = Pick<UserInfo, 'name' | 'avatar' | 'email'>

export type GoogleProfile = {
  avatar: string;
  email: string;
  name: string;
  tokenId: string;
};
