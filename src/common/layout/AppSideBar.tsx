import React from 'react';
import { useRecoilValue } from 'recoil';
import { Link, useLocation } from 'react-router-dom';

import { userPermissionsMapSelector } from 'common/recoil/authen';

type PageObject = { name: string; isEnabled: boolean };

const AppSideBar: React.FC = () => {
  const { pathname } = useLocation();
  const routePage = pathname.split('/')[1];
  const permissionsMap = useRecoilValue(userPermissionsMapSelector);
  const permissionPages = permissionsMap
    ? Object.entries(permissionsMap).map((page): PageObject => {
        return { name: page[0], isEnabled: !!page[1].read };
      })
    : [];

  const renderPageButton = (item: PageObject) => {
    if (!item.isEnabled) return null;
    const { name } = item;

    if (name === 'Riders' || name === 'Dashboard') {
      // TODO: this condition just for demo
      return (
        <li key={item.name} className="my-4 text-gray-800">
          <Link to={`/${name}`} className={`${routePage === name ? 'font-bold text-blue-800' : ''}`}>
            {item.name}
          </Link>
        </li>
      );
    }

    return (
      <li key={item.name} className="my-4 text-gray-400">
        {item.name} - ...
      </li>
    );
  };

  return (
    <aside className="w-56 p-4 bg-white flex-shrink-0">
      <ul>{permissionPages.map((item) => renderPageButton(item))}</ul>
    </aside>
  );
};

export default AppSideBar;
