import React from 'react';
import { useRecoilValue } from 'recoil';
import { Redirect, useLocation } from 'react-router-dom';

import { userInfoSelector } from 'common/recoil/authen';
import PageLoading from 'common/components/PageLoading';
import AppSideBar from './AppSideBar';
import AppHeader from './AppHeader';

const AppLayout: React.FC = ({ children }) => {
  const { pathname } = useLocation();

  const userInfo = useRecoilValue(userInfoSelector);

  const route = pathname.split('/');
  const isLoginPage = route[1] === 'login';

  if (!isLoginPage && !userInfo) {
    return <Redirect to="login" />;
  }

  if (isLoginPage) {
    return (
      <>
        <AppHeader />
        <main>
          <React.Suspense fallback={<PageLoading />}>
            <div>{children}</div>
          </React.Suspense>
        </main>
      </>
    );
  }

  return (
    <>
      <AppHeader />
      <main className="flex bg-gray-100 h-full">
        <AppSideBar />
        <React.Suspense fallback={<PageLoading />}>
          <div className="flex-grow">{children}</div>
        </React.Suspense>
      </main>
    </>
  );
};

export default AppLayout;
