import React from 'react';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import { LogoutIcon } from '@heroicons/react/outline';

import { logOutGoogleAuth } from 'common/utils/google-auth';
import { loginRequestIdState, userInfoBasicSelector } from 'common/recoil/authen';
import Icon from 'common/components/Icon';

import style from '../styles/AppHeader.module.css';

const AppHeader: React.FC = () => {
  const userInfo = useRecoilValue(userInfoBasicSelector);
  const setLoginState = useSetRecoilState(loginRequestIdState);

  const logOut = () => {
    logOutGoogleAuth().then(() => {
      setLoginState('');
    });
  };

  return (
    <header className="p-4 flex justify-between items-center bg-blue-500">
      <div>
        <p className="text-blue-200 font-bold">Latte House - Recoil</p>
      </div>
      {userInfo && (
        <div className="flex items-center text-white">
          <div className={style.avatar}>
            <img src={userInfo.avatar} alt="avatar" />
          </div>
          <span className="mx-4">{userInfo.name}</span>
          <span className="cursor-pointer text-white" onClick={logOut}>
            <Icon element={LogoutIcon} type="light" />
          </span>
        </div>
      )}
    </header>
  );
};

export default AppHeader;
