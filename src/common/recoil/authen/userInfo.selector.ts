import { selector } from 'recoil';

import { getUserInfo } from 'api/authApi';
import { updateHeaders } from 'api/utils';
import { mappingPermission } from 'common/utils/permission';
import { UserInfo, UserInfoBasic } from 'common/models/Authentication';
import { PermissionList, PermissionPage } from 'common/models/Permission';

import { ggProfileSelector } from './googleProfile.selector';
import { loginRequestIdState } from './auth.atom';

const userInfoSelector = selector<UserInfo | null>({
  key: 'userInfoSelector',
  get: async ({ get }) => {
    const loginId = get(loginRequestIdState); // Listen this state for invalidate api purpose
    if (!loginId) return null;

    const ggProfile = get(ggProfileSelector);
    if (ggProfile === null) return null;

    updateHeaders({ Authorization: `Bearer ${ggProfile.tokenId}` });

    try {
      const { data: userInfo } = await getUserInfo();
      const { permissions } = userInfo;
      return { ...userInfo, permissionsMap: mappingPermission(permissions) };
    } catch {
      return null;
    }
  },
});

export const userInfoBasicSelector = selector<UserInfoBasic | null>({
  key: 'userInfoBasicSelector',
  get: ({ get }) => {
    const userInfo = get(userInfoSelector);
    if (!userInfo) return null;

    const { name, email, avatar } = userInfo;
    return { name, email, avatar };
  },
});

export const userPermissionsMapSelector = selector<Record<PermissionPage, PermissionList> | undefined>({
  key: 'userPermissionsMapSelector',
  get: ({ get }) => {
    return get(userInfoSelector)?.permissionsMap;
  },
});

export default userInfoSelector;
