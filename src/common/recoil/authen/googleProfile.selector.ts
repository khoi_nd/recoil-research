import { selector } from 'recoil';

import { GoogleProfile } from 'common/models/Authentication';
import { getGoogleAuthInstance, getGoogleProfile } from 'common/utils/google-auth';
import googleAuthClient from './googleAuthClient.selector';
import { loginRequestIdState } from './auth.atom';

export const ggProfileSelector = selector<GoogleProfile | null>({
  key: 'googleProfileSelector',
  get: async ({ get }) => {
    get(loginRequestIdState);

    const hasAuthClient = get(googleAuthClient);
    const authClient = getGoogleAuthInstance();
    if (hasAuthClient && authClient.isSignedIn.get()) {
      return getGoogleProfile(authClient);
    }
    return null;
  },
});

export default ggProfileSelector;
