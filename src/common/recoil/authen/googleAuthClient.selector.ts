import { selector } from 'recoil';

import { googleAuthClientId } from 'common/constants/config';
import { initGoogleAuth2 } from 'common/utils/google-auth';
import { loadScript } from 'common/utils/load-script';

const googleAuthClientSelector = selector<boolean>({
  key: 'googleScriptStatusSelector',
  get: async () => {
    try {
      await loadScript('https://apis.google.com/js/platform.js');
      await initGoogleAuth2({ client_id: googleAuthClientId });
      return true;
    } catch {
      return false;
    }
  },
});

export default googleAuthClientSelector;
