import { v4 } from 'uuid';
import { atom } from 'recoil';

export const loginRequestIdState = atom<string | ''>({
  key: 'loginRequestId',
  default: v4(),
});