import React from 'react';

import style from '../styles/PageLoading.module.css';

const PageLoading: React.FC = () => {
  return (
    <div className={style.root}>
      <div className={`${style.spinner} animate-spin`}></div>
    </div>
  );
};

export default PageLoading;
