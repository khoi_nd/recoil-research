import React from 'react';
import { Route, useRouteMatch } from 'react-router-dom';

type OwnProps = {
  path: string;
};

type RouteParam = {
  id: string;
};

const RouteForListItem: React.FC<OwnProps> = props => {
  const { path, children } = props;
  const count = React.Children.count(children);
  const match = useRouteMatch<RouteParam>(`${path}/:id`);

  if (count < 2) {
    throw Error('Required 2 children');
  }

  const Components = React.Children.toArray(children);

  if (match) {
    return <Route path={`${path}/:id`}>{Components[1]}</Route>;
  }
  return <Route path={path}>{Components[0]}</Route>;
};

export default RouteForListItem;
