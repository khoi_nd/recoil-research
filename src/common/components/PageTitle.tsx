import React from 'react';

import style from '../styles/PageTitle.module.css';

type Props = {
  text: string;
};

const PageTitle: React.FC<Props> = ({ text }) => {
  return <h1 className={style.PageTitle}>{text}</h1>;
};

export default PageTitle;
