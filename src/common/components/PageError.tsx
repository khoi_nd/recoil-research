import React from 'react';

const PageLoading: React.FC = () => {
  return (
    <div className="py-10 text-center">
      <div className="text-red-400 text-xl font-bold">Oops! something went wrong...</div>
    </div>
  );
};

export default PageLoading;
