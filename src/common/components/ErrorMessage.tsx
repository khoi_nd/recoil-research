import React from 'react';

import { ApiError } from 'common/models/ApiResponse';

type Props = {
  error?: ApiError;
};

const ErrorMessage: React.FC<Props> = ({ error }) => {
  if (!error) return null;
  return <div className="text-red-500 text-right">{error.message || ''}</div>;
};

export default ErrorMessage;
