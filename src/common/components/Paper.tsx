import React from 'react';

const Paper: React.FC = ({ children }) => {
  return <div className="rounded-md bg-white p-4 m-4 mt-0 shadow-md">{children}</div>;
};

export default Paper;
