import React from 'react';
import { Link } from 'react-router-dom';

type Props = {
  nav: {
    text: string;
    link?: string;
  }[];
};

const BreadCrumbs: React.FC<Props> = ({ nav }) => {
  return (
    <div className="p-4">
      {nav.map((item, index) => {
        return (
          <span key={index}>
            {item.link ? (
              <Link className="mr-2 text-gray-800" to={item.link}>
                {item.text}
              </Link>
            ) : (
              <span className="mr-2 text-gray-500">{item.text}</span>
            )}
            {index !== nav.length -1 && <span className="mr-2">/</span>}
          </span>
        );
      })}
    </div>
  );
};

export default BreadCrumbs;
