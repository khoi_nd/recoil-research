import React from 'react';

type Props = {
  title: string;
};

const InfoSection: React.FC<Props> = ({ title, children }) => {
  return (
    <div className="mb-8">
      <h3 className="text-gray-600 text-lg font-semibold">{title}</h3>
      <hr className="mt-2 mb-4" />
      {children}
    </div>
  );
};

export default InfoSection;
