import React from 'react';

type Props = {
  disabled?: boolean;
  type?: 'default' | 'error' | 'light';
  element: React.FunctionComponent<any>;
};

const colorMapping = {
  default: 'gray-700',
  light: 'white',
  error: 'red-400'
}

const Icon: React.FC<Props> = ({ element: Component, disabled, type = 'default' }) => {
  const textColor = disabled ? 'gray-400' : colorMapping[type];
  return <Component className={`w-5 h-5 text-${textColor}`} />;
};

export default Icon;
