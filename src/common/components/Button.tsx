import React from 'react';

import style from '../styles/Button.module.css';

type Props = {
  type?: string;
  onClick?(): void;
};

const Button: React.FC<Props> = ({ children, onClick }) => {
  return (
    <button className={style.Button} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
