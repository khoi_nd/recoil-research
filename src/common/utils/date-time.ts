import { format } from 'date-fns';

export const formatDate = (date: string | Date, pattern = 'dd/MM/yyyy HH:mm:ss') => {
  return date ? format(new Date(date), pattern) : '';
};
