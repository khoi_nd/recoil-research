export const loadScript = (url: string) =>
  new Promise((resolve, reject) => {
    const head = document.getElementsByTagName('head')[0];
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.onload = () => {
      resolve(script);
    };
    script.onerror = (error: any) => {
      reject(error)
    };

    script.src = url;
    head.appendChild(script);
  });
