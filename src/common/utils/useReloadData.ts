/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback } from 'react';
import { RecoilState, useRecoilState } from 'recoil';

export const useReloadData = (requestIdState: RecoilState<number>) => {
  const [state, setState] = useRecoilState(requestIdState);

  const reloadData = useCallback(() => {
    setState(state + 1);
  }, []);

  return reloadData;
};
