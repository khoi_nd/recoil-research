import { PermissionList, PermissionPage } from 'common/models/Permission';

export const mappingPermission = (permissionList: string[]) => {
  const getMCPermission = (permission: string): boolean => {
    return permissionList.includes(permission);
  };

  const PermissionsMap: Record<PermissionPage, PermissionList> = {
    Campaigns: {
      read: getMCPermission('LatteHouse/Campaigns:read'),
      create: getMCPermission('LatteHouse/Campaigns:create'),
      update: getMCPermission('LatteHouse/Campaigns:update'),
      delete: getMCPermission('LatteHouse/Campaigns:delete'),
    },
    Collections: {
      read: getMCPermission('LatteHouse/Collections:read'),
      create: getMCPermission('LatteHouse/Collections:create'),
      update: getMCPermission('LatteHouse/Collections:update'),
      delete: getMCPermission('LatteHouse/Collections:delete'),
    },
    Dashboard: {
      read: getMCPermission('LatteHouse/Dashboard:read'),
      create: getMCPermission('LatteHouse/Dashboard:create'),
      update: getMCPermission('LatteHouse/Dashboard:update'),
      delete: getMCPermission('LatteHouse/Dashboard:delete'),
      readDataDog: getMCPermission('LatteHouse/Dashboard:readDataDog'),
    },
    DynamicConfig: {
      read: getMCPermission('LatteHouse/DynamicConfig:read'),
      create: getMCPermission('LatteHouse/DynamicConfig:create'),
      update: getMCPermission('LatteHouse/DynamicConfig:update'),
      delete: getMCPermission('LatteHouse/DynamicConfig:delete'),
      orderConfig: getMCPermission('LatteHouse/DynamicConfig:getOrderConfig'),
      searchConfig: getMCPermission('LatteHouse/DynamicConfig:getSearchConfig'),
      paymentConfig: getMCPermission('LatteHouse/DynamicConfig:getPaymentConfig'),
      basicConfig: getMCPermission('LatteHouse/DynamicConfig:getBasicConfig'),
      deliveryConfig: getMCPermission('LatteHouse/DynamicConfig:getDeliveryConfig'),
      merchantTypeConfig: getMCPermission('LatteHouse/DynamicConfig:getMerchantTypeConfig'),
      appConfig: getMCPermission('LatteHouse/DynamicConfig:getAppConfig'),
      updateRawConfig: getMCPermission('LatteHouse/DynamicConfig:updateRaw'),
    },
    ManageHome: {
      read: getMCPermission('LatteHouse/ManageHome:read'),
      create: getMCPermission('LatteHouse/ManageHome:create'),
      update: getMCPermission('LatteHouse/ManageHome:update'),
      delete: getMCPermission('LatteHouse/ManageHome:delete'),
      subHomeCreate: getMCPermission('LatteHouse/ManageHome:subHomeCreate'),
      subHomeRead: getMCPermission('LatteHouse/ManageHome:subHomeRead'),
    },
    Orders: {
      read: getMCPermission('LatteHouse/Orders:read'),
      readAllCity: getMCPermission('LatteHouse/Orders:readAllCity'),
      create: getMCPermission('LatteHouse/Orders:create'),
      update: getMCPermission('LatteHouse/Orders:update'),
      delete: getMCPermission('LatteHouse/Orders:delete'),
    },
    Riders: {
      read: getMCPermission('LatteHouse/Riders:read'),
      create: getMCPermission('LatteHouse/Riders:create'),
      update: getMCPermission('LatteHouse/Riders:update'),
      delete: getMCPermission('LatteHouse/Riders:delete'),
      block: getMCPermission('LatteHouse/Riders:block'),
      updateImage: getMCPermission('LatteHouse/Riders:updateImage'),
      updateGender: getMCPermission('LatteHouse/Riders:updateGender'),
      updateIssuedCity: getMCPermission('LatteHouse/Riders:updateIssuedCity'),
      updateIssuedDate: getMCPermission('LatteHouse/Riders:updateIssuedDate'),
    },
    Stores: {
      read: getMCPermission('LatteHouse/Stores:read'),
      create: getMCPermission('LatteHouse/Stores:create'),
      update: getMCPermission('LatteHouse/Stores:update'),
      delete: getMCPermission('LatteHouse/Stores:delete'),
    },
    Users: {
      read: getMCPermission('LatteHouse/Users:read'),
      create: getMCPermission('LatteHouse/Users:create'),
      update: getMCPermission('LatteHouse/Users:update'),
      delete: getMCPermission('LatteHouse/Users:delete'),
    },
    Payments: {
      read: getMCPermission('LatteHouse/Payments:read'),
      create: getMCPermission('LatteHouse/Payments:create'),
      update: getMCPermission('LatteHouse/Payments:update'),
      delete: getMCPermission('LatteHouse/Payments:delete'),
    },
    Cities: {
      HCM: getMCPermission('LatteHouse/Cities/HCM:access'),
      HN: getMCPermission('LatteHouse/Cities/HN:access'),
      DNA: getMCPermission('LatteHouse/Cities/DNA:access'),
      DN: getMCPermission('LatteHouse/Cities/DN:access'),
      BR_VT: getMCPermission('LatteHouse/Cities/BR_VT:access'),
      CT: getMCPermission('LatteHouse/Cities/CT:access'),
      HaiPhong: getMCPermission('LatteHouse/Cities/HaiPhong:access'),
      QuangNam: getMCPermission('LatteHouse/Cities/QuangNam:access'),
      KhanhHoa: getMCPermission('LatteHouse/Cities/KhanhHoa:access'),
      QuangNinh: getMCPermission('LatteHouse/Cities/QuangNinh:access'),
    },
    ReportedOrders: {
      read: getMCPermission('LatteHouse/ReportedOrders:read'),
      update: getMCPermission('LatteHouse/ReportedOrders:update'),
    },
    Frequency: {
      create: getMCPermission('LatteHouse/Frequency:create'),
      read: getMCPermission('LatteHouse/Frequency:read'),
      update: getMCPermission('LatteHouse/Frequency:update'),
      delete: getMCPermission('LatteHouse/Frequency:delete'),
    },
    Coupons: {
      create: getMCPermission('LatteHouse/Coupons:create'),
      read: getMCPermission('LatteHouse/Coupons:read'),
      update: getMCPermission('LatteHouse/Coupons:update'),
      delete: getMCPermission('LatteHouse/Coupons:delete'),
    },
    SurchargeFee: {
      create: getMCPermission('LatteHouse/SurchargeFee:create'),
      read: getMCPermission('LatteHouse/SurchargeFee:read'),
      update: getMCPermission('LatteHouse/SurchargeFee:update'),
      delete: getMCPermission('LatteHouse/SurchargeFee:delete'),
    },
    Settlement: {
      read: getMCPermission('LatteHouse/Settlement:read'),
      readSetting: getMCPermission('LatteHouse/Settlement:readSetting'),
      updateSetting: getMCPermission('LatteHouse/Settlement:updateSetting'),
      createBookRequest: getMCPermission('LatteHouse/Settlement:createBookRequest'),
      reviewBookRequest: getMCPermission('LatteHouse/Settlement:reviewBookRequest'),
      updateBookRequest: getMCPermission('LatteHouse/Settlement:updateBookRequest'),
      requestBankAccountExport: getMCPermission('LatteHouse/Settlement:requestBankAccountExport'),
    },
    ReviewManagement: {
      read: getMCPermission('LatteHouse/ReviewManagement:read'),
      update: getMCPermission('LatteHouse/ReviewManagement:update'),
    },
  };

  return PermissionsMap;
};
