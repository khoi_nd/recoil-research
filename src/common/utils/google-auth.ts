export const initGoogleAuth2 = (param: any) =>
  new Promise((resolve, reject) => {
    try {
      (window as any).gapi.load('auth2', () => {
        const { auth2 } = (window as any).gapi;
        auth2.init(param).then(() => {
          resolve(auth2.getAuthInstance());
        });
      });
    } catch (e) {
      reject(e);
    }
  });

export const getGoogleProfile = (auth2Client: any) => {
  const googleUser = auth2Client.currentUser.get();
  const basicProfile = googleUser.getBasicProfile();
  const tokenId = googleUser.getAuthResponse().id_token;
  return {
    tokenId,
    name: basicProfile.getName(),
    email: basicProfile.getEmail(),
    avatar: basicProfile.getImageUrl(),
  };
};

export const logInGoogleAuth = () =>
  new Promise((resolve, reject) => {
    try {
      const auth2Client = getGoogleAuthInstance();
      auth2Client.signIn();
      resolve('success');
    } catch (e) {
      reject(e);
    }
  });

export const logOutGoogleAuth = () =>
  new Promise(async (resolve, reject) => {
    try {
      const auth2Client = getGoogleAuthInstance();
      await auth2Client.signOut();
      resolve('success');
    } catch (e) {
      reject(e);
    }
  });

export const getGoogleAuthInstance = () => {
  return (window as any).gapi?.auth2?.getAuthInstance();
};
