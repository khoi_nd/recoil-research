import axios from 'axios';
// import { ApiError } from 'common/types/ApiResponse';

let _headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

const API_URI = process.env.LATTE_HOUSE_API_URI || 'https://ch.dev.baemin.vn/v1';

const headers = () => {
  const token = localStorage.getItem('token');
  if (token) {
    return {
      ..._headers,
      'x-access-token': token,
    };
  }
  return _headers;
};

export const updateHeaders = (newHeaders: object) => {
  _headers = { ..._headers, ...newHeaders };
};

export const get = (uri: string, queries?: object, additionalHeader = {}) => {
  const timestamp = new Date().getTime();
  return axios({
    method: 'GET',
    url: `${API_URI}/${uri}`,
    headers: { ...headers(), ...additionalHeader },
    params: { ...queries, ts: timestamp },
  });
};

export const post = (uri: string, queries: object, formIncluded?: boolean) =>
  axios({
    method: 'POST',
    url: `${API_URI}/${uri}`,
    headers: headers(),
    data: formIncluded ? queries : JSON.stringify(queries),
  });

export const put = (uri: string, queries: object, params?: object, formIncluded?: boolean) =>
  axios({
    method: 'PUT',
    url: `${API_URI}/${uri}`,
    headers: headers(),
    data: formIncluded ? queries : JSON.stringify(queries),
    params: { ...params },
  });
