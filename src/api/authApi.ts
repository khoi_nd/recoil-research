import { get } from './utils';

export const getUserInfo = () => get('auth/who');