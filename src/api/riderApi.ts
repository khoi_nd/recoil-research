import { RiderDetail } from 'pages/Rider/models/RiderDetailsModel';
import { get, put } from './utils';

export const getRiders = (payload: { page: number; teamId?: string }) => get('riders', payload);

export const getRider = (id: string) => get(`riders/${id}`);

export const updateRider = (payload: RiderDetail) => {
  const { id, imageFile, ...last } = payload;
  return put(`riders/${id}`, { riderDto: { ...last } });
};
