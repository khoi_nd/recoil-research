import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Router } from 'routes';
import { RecoilRoot } from 'recoil';

import AppLayout from 'common/layout/AppLayout';
import PageLoading from 'common/components/PageLoading';

type AppProps = {};

const App: React.FC<AppProps> = () => {
  return (
    <RecoilRoot>
      <BrowserRouter>
        <React.Suspense fallback={<PageLoading />}>
          <AppLayout>
            <Router />
          </AppLayout>
        </React.Suspense>
      </BrowserRouter>
    </RecoilRoot>
  );
};

export default App;
