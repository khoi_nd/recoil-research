import React from 'react';

import PageTitle from 'common/components/PageTitle';

const Dashboard: React.FC = () => {
  return (
    <div>
      <PageTitle text="Dashboard" />
      <p className="text-center text-gray-600">Coming soon ...</p>
    </div>
  );
};

export default Dashboard;
