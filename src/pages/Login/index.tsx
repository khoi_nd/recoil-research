import React from 'react';
import { v4 } from 'uuid';
import { Redirect } from 'react-router-dom';
import { useRecoilValue, useSetRecoilState } from 'recoil';

import { googleProfileSelector, loginRequestIdState } from 'common/recoil/authen';
import { getGoogleAuthInstance } from 'common/utils/google-auth';
import PageTitle from 'common/components/PageTitle';
import Button from 'common/components/Button';

const LoginPage: React.FC = () => {
  const userInfo = useRecoilValue(googleProfileSelector);
  const setNewLoginRequest = useSetRecoilState(loginRequestIdState);

  const login = () => {
    getGoogleAuthInstance()
      .signIn()
      .then(() => {
        setNewLoginRequest(v4());
      });
  };

  if (userInfo) {
    return <Redirect to="/Dashboard" />;
  }

  return (
    <>
      <PageTitle text="Login Page" />
      <div className="text-center">
        <Button onClick={login}>Login with Google</Button>
      </div>
    </>
  );
};

export default LoginPage;
