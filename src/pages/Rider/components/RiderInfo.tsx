import React from 'react';
import { useRecoilValue } from 'recoil';

import ErrorMessage from 'common/components/ErrorMessage';
import InfoSection from 'common/components/InfoSection';
import Paper from 'common/components/Paper';
import { RiderBasicInfo } from '../models/RiderDetailsModel';
import { riderEditState } from '../recoil/riderEditMode.atom';
import { riderResponseErrorState } from '../recoil/riderState.atom';
import { riderAvartarSelector, riderBasicInfoSelector, riderResponseSelector } from '../recoil/rider.selector';
import { useUpdateDetails } from '../callbacks/useUpdateDetails';
import RiderBasicInfoList from './RiderBasicInfoList';
import RiderDetailHead from './RiderDetailHead';
import Toolbar from './Toolbar';

type Props = { id: string };

const RiderInfo: React.FC<Props> = ({ id }) => {
  const postUpdateData = useUpdateDetails(id);
  const isEditMode = useRecoilValue(riderEditState);
  const riderDetails = useRecoilValue(riderResponseSelector(id));
  const riderAvatar = useRecoilValue(riderAvartarSelector(id));
  const riderBasicInfo = useRecoilValue(riderBasicInfoSelector(id));
  const riderErrorState = useRecoilValue(riderResponseErrorState(id));
  const [basicInfo, setBasicInfo] = React.useState(riderBasicInfo);

  React.useEffect(() => {
    if (!isEditMode) {
      setBasicInfo(riderBasicInfo);
    }
  }, [isEditMode, riderBasicInfo]);

  const updateHandle = () => {
    postUpdateData({ ...riderDetails, ...basicInfo });
  };

  const onChangeHandle = (key: keyof RiderBasicInfo, value: string) => {
    setBasicInfo({ ...basicInfo, [key]: value });
  };

  return (
    <Paper>
      <Toolbar onUpdate={updateHandle} />
      <RiderDetailHead avatar={riderAvatar} />
      <InfoSection title="Rider Information">
        <RiderBasicInfoList basicInfo={basicInfo!} onChange={onChangeHandle} />
      </InfoSection>
      <ErrorMessage error={riderErrorState} />
    </Paper>
  );
};

export default RiderInfo;
