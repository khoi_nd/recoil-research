import React from 'react';

import style from '../styles/RiderBasicInfo.module.css';

type Props = {
  title: string;
  value?: string;
};

const InfoItem: React.FC<Props> = ({ title, value = '' }) => {
  return (
    <div className={style.item}>
      <p className="font-semibold text-gray-600">{title}:</p>
      <p className="text-gray-600">{value}</p>
    </div>
  );
};

export default InfoItem;
