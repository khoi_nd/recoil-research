import React from 'react';
import { useRecoilValue } from 'recoil';

import { RiderBasicInfo } from '../models/RiderDetailsModel';
import { riderEditState } from '../recoil/riderEditMode.atom';
import style from '../styles/RiderBasicInfo.module.css';
import InfoInputField from './InfoInputField';
import InfoItem from './InfoItem';

type Props = {
  basicInfo: RiderBasicInfo;
  onChange(key: keyof RiderBasicInfo, value: string): void;
};

const RiderBasicInfoList: React.FC<Props> = ({ basicInfo, onChange }) => {
  const isEditMode = useRecoilValue(riderEditState);

  const updateValue = (key: keyof RiderBasicInfo) => (value: string) => {
    onChange(key, value);
  };

  return (
    <div className={style.grid}>
      <InfoItem title="Rider id:" value={basicInfo.id} />
      <InfoItem title="Bros id:" value={basicInfo.username} />
      <InfoItem title="Fleed id:" value={basicInfo.fleetId} />
      <InfoItem title="Full name:" value={basicInfo.fullName} />

      {isEditMode ? (
        <>
          <InfoInputField title="First name:" value={basicInfo.firstName} onChange={updateValue('firstName')} />
          <InfoInputField title="Middle name:" value={basicInfo.middleName} onChange={updateValue('middleName')} />
          <InfoInputField title="Last name:" value={basicInfo.lastName} onChange={updateValue('lastName')} />
          <InfoInputField title="Phone number:" value={basicInfo.phone} onChange={updateValue('phone')} />
          <InfoInputField title="Phone number:" value={basicInfo.email} onChange={updateValue('email')} />
        </>
      ) : (
        <>
          <InfoItem title="First name:" value={basicInfo.firstName} />
          <InfoItem title="Middle name:" value={basicInfo.middleName} />
          <InfoItem title="Last name:" value={basicInfo.lastName} />
          <InfoItem title="Phone number:" value={basicInfo.phone} />
          <InfoItem title="Phone number:" value={basicInfo.email} />
        </>
      )}
    </div>
  );
};

export default RiderBasicInfoList;
