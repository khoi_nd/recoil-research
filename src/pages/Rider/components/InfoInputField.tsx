import React from 'react';

import style from '../styles/RiderBasicInfo.module.css';

type Props = {
  title: string;
  value?: string;
  onChange(val: string): void;
};

const InfoItem: React.FC<Props> = ({ title, onChange, value = '' }) => {
  const changeHandle = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.currentTarget.value);
  };

  return (
    <div className={style.item}>
      <p className="font-semibold text-gray-600">{title}:</p>
      <input className="border-b border-gray-300" value={value} onChange={changeHandle} />
    </div>
  );
};

export default InfoItem;
