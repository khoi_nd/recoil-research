import React from 'react';

type Props = { avatar: string };

const RiderDetailHead: React.FC<Props> = ({ avatar }) => {
  return (
    <div className="mb-8">
      <div className="avatar">
        <img src={avatar} className="w-32 shadow-md" alt="rider avatar" />
      </div>
    </div>
  );
};

export default RiderDetailHead;
