import React from 'react';
import { useRecoilState, useRecoilValue } from 'recoil';
import { PencilIcon, UploadIcon, XCircleIcon } from '@heroicons/react/outline';

import Icon from 'common/components/Icon';
import { riderEditState } from '../recoil/riderEditMode.atom';
import { riderPermissionSelector } from '../recoil/riderPermissions.selector';

type Props = { onUpdate(): void };

const Toolbar: React.FC<Props> = ({ onUpdate }) => {
  const [isEditMode, setEditMode] = useRecoilState(riderEditState);
  const riderPermission = useRecoilValue(riderPermissionSelector);

  if (!riderPermission.update) return null;
  if (!isEditMode) {
    return (
      <div className="text-right">
        <button
          onClick={() => {
            setEditMode(true);
          }}
        >
          <Icon element={PencilIcon} />
        </button>
      </div>
    );
  }

  return (
    <div className="text-right">
      <button
        className="mr-2"
        onClick={() => {
          onUpdate();
        }}
      >
        <Icon element={UploadIcon} />
      </button>
      <button
        onClick={() => {
          setEditMode(false);
        }}
      >
        <Icon element={XCircleIcon} type="error" />
      </button>
    </div>
  );
};

export default Toolbar;
