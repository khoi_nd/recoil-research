import { selectorFamily } from 'recoil';

import { getRider } from 'api/riderApi';
import { RiderBasicInfo, RiderDetail } from '../models/RiderDetailsModel';
import { riderRequestIdState } from './riderState.atom';

export const riderResponseSelector = selectorFamily<RiderDetail, string>({
  key: 'riderResponseSelector',
  get:
    (id) =>
    async ({ get }) => {
      get(riderRequestIdState(id));
      try {
        const response = await getRider(id);
        return response.data;
      } catch (e) {
        throw e;
      }
    },
});

export const riderAvartarSelector = selectorFamily<string, string>({
  key: 'riderAvartarSelector',
  get:
    (id) =>
    ({ get }) => {
      const riderData = get(riderResponseSelector(id));
      return riderData?.imageUrl || '';
    },
});

export const riderBasicInfoSelector = selectorFamily<RiderBasicInfo, string>({
  key: 'riderBasicInfoSelector',
  get:
    (riderId) =>
    ({ get }) => {
      const riderData = get(riderResponseSelector(riderId));
      const { username, fullName, firstName, middleName, lastName, fleetId, phone, email, id, gender, license, dateOfBirth, contractType } =
        riderData;
      return {
        username,
        fullName,
        firstName,
        middleName,
        lastName,
        fleetId,
        phone,
        email,
        id,
        gender,
        license,
        dateOfBirth,
        contractType,
      };
    },
});
