import { selector } from 'recoil';

import { getRiders } from 'api/riderApi';
import { ListApiResponse } from 'common/models/ApiResponse';
import { Rider } from '../models/RiderModel';
import { riderListRequestIdState } from './ridersListState.atom';

export const ridersListResponseSelector = selector<ListApiResponse<Rider>>({
  key: 'ridersListResponseSelector',
  get: async ({ get }) => {
    const id = get(riderListRequestIdState);
    if (!id) return;

    try {
      const response = await getRiders({ page: 1 });
      return response.data;
    } catch (e) {
      return e;
    }
  },
});

export const riderListSelector = selector<Rider[]>({
  key: 'ridersListSelector',
  get: ({ get }) => {
    const ridersListResponse = get(ridersListResponseSelector);
    return ridersListResponse?.docs || [];
  },
});
