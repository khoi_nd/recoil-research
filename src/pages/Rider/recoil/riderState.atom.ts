import { atomFamily } from 'recoil';

import { ApiError } from 'common/models/ApiResponse';

export const riderRequestIdState = atomFamily<number, string>({
  key: 'riderRequestId',
  default: 0,
});

export const riderResponseErrorState = atomFamily<ApiError | undefined, string>({
  key: 'riderResponseError',
  default: id => undefined
})
