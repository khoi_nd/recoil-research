import { selector } from 'recoil';

import { userPermissionsMapSelector } from 'common/recoil/authen';
import { RiderPermission } from '../models/RiderPermissionModel';

const defaultRiderPermission: RiderPermission = {
  block: false,
  create: false,
  delete: false,
  read: false,
  update: false,
  updateImage: false,
  updateGender: false,
  updateIssuedCity: false,
  updateIssuedDate: false,
};

export const riderPermissionSelector = selector<RiderPermission>({
  key: 'riderPermissionSelector',
  get: ({ get }) => {
    const permission = get(userPermissionsMapSelector);
    return permission?.Riders as RiderPermission || defaultRiderPermission;
  },
});
