import { atom } from 'recoil';

export const riderListRequestIdState = atom<number>({
  key: 'riderListRequestId',
  default: 0,
});
