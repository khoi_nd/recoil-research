import { atom } from 'recoil';

export const riderEditState = atom<boolean>({
  key: 'riderEditState',
  default: false,
});