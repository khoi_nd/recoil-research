import React from 'react';
import { useParams } from 'react-router-dom';
import { useRecoilValueLoadable, useSetRecoilState } from 'recoil';

import { RequestStatus } from 'common/constants/api';
import BreadCrumbs from 'common/components/Breadcrumbs';
import PageLoading from 'common/components/PageLoading';
import PageError from 'common/components/PageError';
import { riderBasicInfoSelector } from './recoil/rider.selector';
import { riderEditState } from './recoil/riderEditMode.atom';
import RiderInfo from './components/RiderInfo';

type RiderParams = {
  id: string;
};

const Rider: React.FC = () => {
  const { id } = useParams<RiderParams>();
  const { state, contents } = useRecoilValueLoadable(riderBasicInfoSelector(id));
  const setEditMode = useSetRecoilState(riderEditState);

  React.useEffect(() => {
    return () => {
      setEditMode(false);
    };
  }, [setEditMode]);

  if (state === RequestStatus.LOADING) {
    return <PageLoading />;
  }

  if (state === RequestStatus.ERROR) {
    return <PageError />;
  }

  return (
    <div>
      <BreadCrumbs nav={[{ text: 'Riders', link: '/Riders' }, { text: contents.fullName }]} />
      <RiderInfo id={id} />
    </div>
  );
};

export default Rider;
