import { useRecoilCallback } from 'recoil';

import { updateRider } from 'api/riderApi';
import { useReloadData } from 'common/utils/useReloadData';
import { RiderDetail } from '../models/RiderDetailsModel';
import { riderEditState } from '../recoil/riderEditMode.atom';
import { riderRequestIdState, riderResponseErrorState } from '../recoil/riderState.atom';

export const useUpdateDetails = (id: string) => {
  const reloadData = useReloadData(riderRequestIdState(id));

  return useRecoilCallback(({ set }) => async (params: RiderDetail) => {
    try {
      await updateRider(params);
      reloadData();
      set(riderEditState, false);
    } catch (e) {
      set(riderResponseErrorState(id), e);
    }
  });
}