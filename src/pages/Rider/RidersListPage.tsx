import React from 'react';
import { Link } from 'react-router-dom';
import { useRecoilValue } from 'recoil';

import { useReloadData } from 'common/utils/useReloadData';
import { formatDate } from 'common/utils/date-time';
import PageTitle from 'common/components/PageTitle';
import Paper from 'common/components/Paper';
import { riderListRequestIdState } from './recoil/ridersListState.atom';
import { riderListSelector } from './recoil/ridersList.selector';

const RidersList: React.FC = () => {
  const riders = useRecoilValue(riderListSelector);
  const reloadData = useReloadData(riderListRequestIdState);

  React.useEffect(() => {
    reloadData();
  }, [reloadData]);

  const riderItemLink = (id: string) => (
    <Link to={`/Riders/${id}`} className="text-blue-500">
      {id}
    </Link>
  );

  return (
    <>
      <PageTitle text="Riders list" />

      <Paper>
        <table className="table-auto w-full">
          <thead>
            <tr>
              <th className="px-4 py-1 text-left">Id</th>
              <th className="px-4 py-1 text-left">Name</th>
              <th className="px-4 py-1 text-left">Email</th>
              <th className="px-4 py-1 text-left">Phone</th>
              <th className="px-4 py-1 text-right">Join Date</th>
            </tr>
          </thead>
          <tbody>
            {riders.map((item) => (
              <tr className="even:bg-gray-100" key={item.id}>
                <td className="px-4 py-1 text-sm">{riderItemLink(item.id)}</td>
                <td className="px-4 py-1 text-sm">{item.username}</td>
                <td className="px-4 py-1 text-sm">{item.email}</td>
                <td className="px-4 py-1 text-sm">{item.phone}</td>
                <td className="px-4 py-1 text-sm text-right">{formatDate(item.createdAt)}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </Paper>
    </>
  );
};

export default RidersList;
