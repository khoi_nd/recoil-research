export type Rider = {
  createdAt: string;
  deleted: boolean;
  email: string;
  id: string;
  link: string;
  fullName: string;
  phone: string;
  fleetId: string;
  updatedAt: string;
  username?: string;
};