export type Delivery = {};

export type FromUser = {
  price: string;
};

export type Merchant = {
  id: string;
  name: string;
};

export type Order = {
  id: string;
  createdAt: string;
};

export interface RiderOrder {
  delivery: Delivery;
  fromUser: FromUser;
  merchant: Merchant;
  order: Order;
}
