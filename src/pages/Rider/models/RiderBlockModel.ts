export enum RiderBlockStatusCode {
  BLOCK = 0,
  UNBLOCK = 1,
  NOT_AVAILABLE = 'NA',
}

export type RiderBlockInfo = {
  fleetId: string;
  blockReason?: string;
  unblockOnDate?: string | Date;
};

export type RiderUnblockInfo = Pick<RiderBlockInfo, 'fleetId'>;

export enum RiderBlockType {
  TEMPORARY = 'TEMPORARY',
  PERNAMENT = 'PERNAMENT',
}
