export type RiderPermission = {
  block: boolean;
  create: boolean;
  delete: boolean;
  read: boolean;
  update: boolean;
  updateGender: boolean;
  updateImage: boolean;
  updateIssuedCity: boolean;
  updateIssuedDate: boolean;
};
