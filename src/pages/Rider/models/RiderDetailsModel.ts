import { Cities } from 'common/models/Cities';
import { RiderBlockStatusCode } from './RiderBlockModel';
import { RiderOrder } from './RiderOrdersModel';
import { Rider } from './RiderModel';

export type RiderRateModel = {
  rateAverage: number;
  rateCount: number;
  rateSum: number;
};

export enum RiderAvailableStatusCode {
  OFFLINE = 0,
  ONLINE = 1,
  NOT_AVAILABLE = 'NA',
}

export enum RiderDeliveryStatus {
  FREE = 0,
  BUSY = 1,
  NOT_AVAILABLE = 'NA',
}

export interface RiderDetail extends Rider {
  _id: string;
  contractType?: string;
  dateOfBirth?: string;

  firstName: string;
  middleName?: string;
  lastName: string;
  gender?: string;
  imageUrl: string;
  imageFile?: File;
  phone: string;
  email: string;
  license: string;
  lastFetched: string;
  issuedId?: string;
  issuedDate?: string;
  issuedCity?: Cities;
  issuedAddress?: string;
  bankName?: string;
  bankBranch?: string;
  bankAccountName?: string;
  bankAccountNumber?: string;
  orders: RiderOrder[];
  csvData?: string;
  teamId: string;
  teamName: string;
  rate?: RiderRateModel; // TODO: should convert to required from optional
  blockStatus: RiderBlockStatusCode;
  isAvailable: RiderAvailableStatusCode;
  status: RiderDeliveryStatus;
  blockReason: string;
  unblockOnDate?: string;
  tags: string[];
}

export type RiderBasicInfo = Pick<
  RiderDetail,
  | 'username'
  | 'fullName'
  | 'firstName'
  | 'middleName'
  | 'lastName'
  | 'fleetId'
  | 'phone'
  | 'email'
  | 'id'
  | 'gender'
  | 'license'
  | 'dateOfBirth'
  | 'contractType'
>;
