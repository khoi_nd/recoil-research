import React from 'react';
import { Redirect } from 'react-router-dom';
import { useRecoilValue } from 'recoil';

import RouteForListItem from 'common/components/RouteForListItem';
import { riderPermissionSelector } from './recoil/riderPermissions.selector';
import RidersList from './RidersListPage';
import Rider from './RiderPage';

const RiderPage: React.FC = () => {
  const riderPermission = useRecoilValue(riderPermissionSelector);

  if (!riderPermission.read) {
    return <Redirect to="/dashboard" />
  }

  return (
    <div>
      <RouteForListItem path="/Riders">
        <RidersList />
        <Rider />
      </RouteForListItem>
    </div>
  );
};

export default RiderPage;
