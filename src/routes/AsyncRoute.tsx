import React from 'react';

type AsyncRouteType = {
  pageName: string;
  Component: React.LazyExoticComponent<React.ComponentType<any>>;
};

const AsyncRoute: React.FC<AsyncRouteType> = ({ Component, pageName }) => {
  React.useEffect(() => {
    document.title = pageName;
  }, [pageName]);

  return <Component />;
};

export default AsyncRoute;
