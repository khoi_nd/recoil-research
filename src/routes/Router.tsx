import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import AsyncRoute from './AsyncRoute';

const LoginPage = React.lazy(() => import('pages/Login' /* webpackChunkName: "Login" */));
const DashboardPage = React.lazy(() => import('pages/Dashboard' /* webpackChunkName: "Dashboard" */));
const RiderPage = React.lazy(() => import('pages/Rider' /* webpackChunkName: "Rider" */));

export const Router: React.FC = () => (
  <Switch>
    <Redirect exact from="/" to="/dashboard" />
    <Route exact path="/login" render={() => <AsyncRoute pageName="Login | RecoilPOC" Component={LoginPage} />} />
    <Route exact path="/dashboard" render={() => <AsyncRoute pageName="Dashboard | RecoilPOC" Component={DashboardPage} />} />
    <Route path="/riders" render={() => <AsyncRoute pageName="Riders | RecoilPOC" Component={RiderPage} />} />
  </Switch>
);
